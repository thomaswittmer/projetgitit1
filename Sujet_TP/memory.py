import numpy as np
import random

"""
@author vcoindet
Jeu de memory
"""

Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i] bla
        j = int(random.random() * (i+1))
        Tab[i], Tab[j] = Tab[j], Tab[i]
    return Tab

def carte_cache(Tab):
    """
    This function create a list of the cards hidden in the game.
    ------
    Arg : - Tab : a list of cards to be used in the game with pairs of matching cards.
    ------
    Returns : - Tab_cache : a list of the cards hidden in the game
    
    """
    Tab_cache = len(Tab) * [0]
    return Tab_cache



def choisir_cartes(Tab):
    c1 = int(input("Choisissez une carte : "))
    print(Tab[c1])
    c2 = int(input("Choisissez une deuxieme carte : "))
    while c1 == c2:
        print("Erreur, la deuxieme carte ne peut être la même que la premiere ! ")
        c2 = int(input("Veuillez choisir une deuxieme carte différente de la premiere : "))
    print(Tab[c2])
    return [c1,c2]


def retourne_carte (c1, c2, Tab, Tab_cache):    #TODO
    """
    La fonction retourne les cartes correspondant aux indices c1 et c2 dans Tab_cache, en révélant leur valeur.
    Si les cartes ont la même valeur, elles restent retournées est leur valeur est ajoutée à un compteur de paires trouvées.
    Sinon, les cartes restent cachées.
    ------
    Arg : 
        - c1 : L'indice de la première carte choisie par le joueur
        - c2 : L'indice de la seconde carte choisie par le joueur
        - Tab : La liste des cartes mélangées
    ------
        - Tab_cache : La liste des cartes cachées
    Returns : - None 
        
    """
    Tab_cache[c1] = Tab[c1]
    Tab_cache[c2] = Tab[c2]
    return Tab_cache
    

def jouer(Tab):
    """
    This function plays a memory game with the given list of cards
    ------
    Arg : - Tab : a list of cards to be used in the game with pairs of matching cards.
    ------
    Returns : - None
    """
    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)

    [c1, c2] = choisir_cartes(Tab)
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
    print(Tab_cache)
    
    while 0 in Tab_cache:
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        
        print(Tab_cache)
        
    print("Bravo tu as gagné!!!")

if __name__ == "__main__" :
    #Test melange_carte
    Tabl_melangee = melange_carte(Tabl)
    print(Tabl)
    
    #Test carte_cache
    Tabl_cachee = carte_cache(Tabl_melangee)
    print(Tabl_cachee)
    
    #Test choisir_carte
    
    #Test retourne_carte
    retourne_carte(1,2,Tabl_melangee, Tabl_cachee)
    print(Tabl_cachee)
    
#jouer(Tabl)